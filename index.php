<?php 
require_once 'bootstrap.php';
$hangman = PhilKershaw\Hangman::getInstance(array('antidisestablishmentarianism', 'bikes', 'cheeseburgers', 'crackerjack', 'fusion', 'mammalian'), 10);
$hangman->run();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Hangman Game - Phil Kershaw</title>
</head>
<body>
	<header>
		<hgroup>
			<h1>Hangman Game</h1>
			<h2>Guess correctly or be sentenced to death by hanging</h2>
		</hgroup>
	</header>
	<div class="body">
		<section id="letters">
			<p><?php echo $hangman->message; ?></p>
			<h2><?php echo $hangman->secret; ?></h2>
		</section>
		<form method="POST">
			<input type="text" name="guess" placeholder="Enter a letter to guess." maxlength="1" />
			<input type="submit" value="Guess" />
		</form>
	</div>
	<footer>
		<h6>Created by Phil Kershaw</h6>
	</footer>
</body>
</html>